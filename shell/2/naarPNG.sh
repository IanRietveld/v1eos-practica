
#!/usr/bin/env bash

for Afbeelding in $1*.jpg ; do
    convert "$Afbeelding" -resize 128 x 128\>  "${Afbeelding%.*}.png" ;
done

#Call on with: sudo ./naarPNG.sh Afbeeldingen/