case "$1" in
        *.py)
            python3 $1 ;;
        *.sh)
            bash $1 ;;
        *.cc)
            cat $1 ;;
        *)
            echo $"Usage: {bestand.py|.sh|.cc}"
            exit 1
esac

# Call on with: ./voerUit.sh bestanden/ "file.py, .sh, .cc"