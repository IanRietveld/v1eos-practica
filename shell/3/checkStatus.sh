#!/usr/bin/env bash

for file in $1*; do
    if $2 "$file"; then
        echo "$file Execute succeeded!" >> "$3";
    else
        echo "$file Execute failed!" >> "$3"; 
    fi

done 

#Call on with: sudo ./checkStatus.sh bestanden/python3 logbestand.txt
