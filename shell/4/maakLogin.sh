# #!/usr/bin/env bash

echo "Enter your username: ";
read username;

#Enter username
if [ ! "$username" ]; then
    username=$(whoami) ;
    echo "No username entered, applying standard username: $username";

else 
    echo "Username: $username";
    
fi

echo "Enter your password: ";
read password1;

echo "Confirm password:";
read password2; 


while [ "$password1" != "$password2" -o ! ${#password2} -ge 8 ]; do

    echo "Enter your password: ";
    read password1;

    echo "Confirm password: ";
    read password2; 

done 

passwordhash=$(echo "$password2" | md5sum);

printf "Username: $username \nPassword: $passwordhash\n" >> $1;


